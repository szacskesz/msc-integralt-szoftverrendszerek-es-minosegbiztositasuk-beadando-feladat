﻿using System;
using WebFootball.Models;

namespace WebFootball
{
    public class GlobalConfiguration
    {
        public const int FieldWidth = 140;
        public const int FieldHeight = 90;
        public const float ballDistance = 5;

        public static int PlayTime => (int)TimeSpan.FromMinutes(10).TotalMilliseconds;
        public static Point2d BallPosition => new Point2d(10, FieldHeight / 2);

    }
}
