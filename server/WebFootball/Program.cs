using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

using WebFootball.Models;

namespace WebFootball
{
    public class Program
    {
        public static State GameState { get; } = new State()
        {
            remainingTime = GlobalConfiguration.PlayTime,
            initialBall = new GameObject
            {
                speed = 0f,
                position = GlobalConfiguration.BallPosition,
                direction = new Point2d(0,0)
            },
            PlayerMaxSpeed = 10f,
            BallMaxSpeed = 10f,
            phase = Phase.WAIT_FOR_CONNECTION


        };

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
