using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Diagnostics;
using System.Text.Json;
using WebFootball.Models;
using WebFootball.Socket;

namespace WebFootball
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            services.AddHostedService<SendState>();

            services.AddWebSocketManager();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();

            app.UseRouting();

            var serviceScopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            var serviceProvider = serviceScopeFactory.CreateScope().ServiceProvider;

            app.UseWebSockets();
            app.MapWebSocketManager("/ws", serviceProvider.GetService<FootballMessageHandler>());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });

            initGameData();
        }
        void initGameData()
        {
            float initBallSpeed = (float)Configuration.GetValue(typeof(float), "BallSpeed");
            Point2d initBallPosition = JsonSerializer.Deserialize<Point2d>(Configuration.GetValue(typeof(string), "BallPosition").ToString());
            Point2d initBallDirection = JsonSerializer.Deserialize<Point2d>(Configuration.GetValue(typeof(string), "BallDirection").ToString());
            long initRemainingTime = (long)Configuration.GetValue(typeof(long), "remainingTime");

            float initPlayerMaxSpeed = (float)Configuration.GetValue(typeof(float), "PlayerMaxSpeed");
            float initBallMaxSpeed = (float)Configuration.GetValue(typeof(float), "BallMaxSpeed");

            Program.GameState.remainingTime = initRemainingTime;
            Program.GameState.initialBall.speed = initBallSpeed;
            Program.GameState.initialBall.position = initBallPosition;
            Program.GameState.initialBall.direction = initBallDirection;

            Program.GameState.PlayerMaxSpeed = initPlayerMaxSpeed;
            Program.GameState.BallMaxSpeed = initBallMaxSpeed;
           
        }
        
    }
}
