﻿using Microsoft.Extensions.Hosting;

using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

using WebFootball.Models;

namespace WebFootball
{
    public sealed class SendState : IHostedService, IDisposable
    {
        private Timer _timer;
        private long timeStamp;
        private readonly FootballMessageHandler messageHandler;

        private readonly JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };

        public SendState(FootballMessageHandler messageHandler)
        {
            options.Converters.Add(new JsonStringEnumConverter());
            this.messageHandler = messageHandler;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            
            _timer = new Timer(Send, null, TimeSpan.Zero, TimeSpan.FromMilliseconds(1000 / 60));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public async void Send(object state)
        {

            long elapsedTime;
            long actualTimeStamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();

            elapsedTime = timeStamp == 0
                ? 0
                : (actualTimeStamp - timeStamp);

            timeStamp = actualTimeStamp;


            if (Program.GameState.phase != Phase.PLAY)
            {
                return;
            }

            //Update Positions
            //Team1
            foreach (GameObject player in Program.GameState.team1.currentPlayers)
            {
                if ((player.position.x += player.direction.x * player.speed * elapsedTime) > 140)
                {
                    player.position.x = 140;
                }
                if ((player.position.y += player.direction.y * player.speed * elapsedTime) > 90)
                {
                    player.position.y = 90;
                };
            }
            //Team2
            foreach (GameObject player in Program.GameState.team2.currentPlayers)
            {
                if ((player.position.x += player.direction.x * player.speed * elapsedTime) > 140)
                {
                    player.position.x = 140;
                }
                if ((player.position.y += player.direction.y * player.speed * elapsedTime ) > 90)
                {
                    player.position.y = 90;
                }
                if ((player.position.x += player.direction.x * player.speed * elapsedTime) < 0)
                {
                    player.position.x = 0;
                }
                if ((player.position.y += player.direction.y * player.speed * elapsedTime) < 0)
                {
                    player.position.y = 0;
                }
            }
            //Ball
            if ((Program.GameState.currentBall.position.x += Program.GameState.currentBall.direction.x * Program.GameState.currentBall.speed * elapsedTime) > 140)
            {
                Program.GameState.currentBall.position.x = 140;
                Program.GameState.currentBall.direction.x *= -1;
            }
            if ((Program.GameState.currentBall.position.y += Program.GameState.currentBall.direction.y * Program.GameState.currentBall.speed * elapsedTime) > 90)
            {
                Program.GameState.currentBall.position.y = 90;
                Program.GameState.currentBall.direction.y *= -1;
            }
            if ((Program.GameState.currentBall.position.x += Program.GameState.currentBall.direction.x * Program.GameState.currentBall.speed * elapsedTime) < 0)
            {
                Program.GameState.currentBall.position.x = 0;
                Program.GameState.currentBall.direction.x *= -1;
            }
            if ((Program.GameState.currentBall.position.y += Program.GameState.currentBall.direction.y * Program.GameState.currentBall.speed * elapsedTime) < 0)
            {
                Program.GameState.currentBall.position.y = 0;
                Program.GameState.currentBall.direction.y *= -1;
            }

            //Slow down the ball 
            if (Program.GameState.currentBall.speed > 0)
            {
                Program.GameState.currentBall.speed -= elapsedTime / 1000f;

                if (Program.GameState.currentBall.speed < 0)
                {
                    Program.GameState.currentBall.speed = 0;
                }
            }

            //Check the goal and reset the objects
            if (Program.GameState.currentBall.position.x <= 5 && Program.GameState.currentBall.position.x >= 2.5 && Program.GameState.currentBall.position.y <= 48.66 && Program.GameState.currentBall.position.y >= 41.34)
            {
                goal(Side.RIGHT);

            }else if (Program.GameState.currentBall.position.x <= 137.5 && Program.GameState.currentBall.position.x >= 135 && Program.GameState.currentBall.position.y <= 48.66 && Program.GameState.currentBall.position.y >= 41.34)
            {
                goal(Side.LEFT);          
            }

            //Timer
            Program.GameState.remainingTime -= elapsedTime;

            await messageHandler.SendState();


        }

        public void reset()
        {
            Program.GameState.team1.currentPlayers = JsonSerializer.Deserialize<List<GameObject>>(JsonSerializer.Serialize(Program.GameState.team1.initialPalyers));
            Program.GameState.team2.currentPlayers = JsonSerializer.Deserialize<List<GameObject>>(JsonSerializer.Serialize(Program.GameState.team2.initialPalyers));
            Program.GameState.currentBall = JsonSerializer.Deserialize<GameObject>(JsonSerializer.Serialize(Program.GameState.initialBall));
        }

        public void goal(Side side)
        {
            if (Program.GameState.team1.side == side)
            {
                Program.GameState.team1.score += 1;
               
            }
            else
            {
                Program.GameState.team2.score += 1;
            }
            reset();
        }
   
        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
