﻿using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;

using WebFootball.Socket;
using System.Text.Json;
using WebFootball.Models;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using System;
using System.Linq;

namespace WebFootball
{
    public class FootballMessageHandler : WebSocketHandler
    {


        private readonly JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,


        };

        public FootballMessageHandler(ConnectionManager webSocketConnectionManager) : base(webSocketConnectionManager)
        {
            options.Converters.Add(new JsonStringEnumConverter());
        }

        public override async Task OnConnected(WebSocket socket)
        {
            await base.OnConnected(socket);

            var socketId = WebSocketConnectionManager.GetId(socket);
            await SendMessageToAllAsync($"{socketId} is now connected");
        }

        public override async Task ReceiveAsync(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            //var socketId = WebSocketConnectionManager.GetId(socket);
            var message = Encoding.UTF8.GetString(buffer, 0, result.Count);

            var type = JsonSerializer.Deserialize<Message>(message, options);
            switch (type.Type)
            {
                case "CONNECT":
                    var connect = JsonSerializer.Deserialize<Connect>(message, options);
                    SetTeam(connect);
                    break;
                case "UPDATE_TEAM":
                    var team = JsonSerializer.Deserialize<UpdateTeam>(message, options);
                    UpdateTeam(team);
                    break;
                case "UPDATE_PLAYERS":
                    var players = JsonSerializer.Deserialize<UpdatePlayers>(message, options);
                    UpdatePlayer(players);
                    break;
            }

            //await SendMessageToAllAsync(JsonSerializer.Serialize(Program.GameState, options));
            await SendState();
        }

        private static void SetTeam(Connect connect)
        {
            Team team = new()
            {
                name = connect.Name,
                id = connect.Id,
                isReady = false,
                score = 0,
                currentPlayers = new List<GameObject>(),
                initialPalyers = new List<GameObject>()
            };

            if (Program.GameState.team1 == null)
            {
                team.side = Side.LEFT;
                Program.GameState.team1 = team;
            }
            else
            {
                team.side = Side.RIGHT;
                Program.GameState.phase = Phase.WAIT_FOR_READY;
                Program.GameState.team2 = team;
            }
        }

        private static void UpdateTeam(UpdateTeam team)
        {
            if (Program.GameState.team1.id == team.Id)

            {
                if (Program.GameState.team1.side == Side.LEFT)
                {
                    if (team.InitialPlayers.Any(p => p.position.x > 70))
                    {
                        return;
                    }
                }
                else
                {
                    if (team.InitialPlayers.Any(p => p.position.x < 70))
                    {
                        return;
                    }
                }

                Program.GameState.team1.isReady = team.IsReady;
                Program.GameState.team1.initialPalyers = team.InitialPlayers;
            }
            else
            {
                if (Program.GameState.team2.side == Side.LEFT)
                {
                    if (team.InitialPlayers.Any(p => p.position.x > 70))
                    {
                        return;
                    }
                }
                else
                {
                    if (team.InitialPlayers.Any(p => p.position.x < 70))
                    {
                        return;
                    }
                }
                Program.GameState.team2.isReady = team.IsReady;
                Program.GameState.team2.initialPalyers = team.InitialPlayers;
            }

            if (Program.GameState.team1 is not null && Program.GameState.team2 is not null && Program.GameState.team1.isReady && Program.GameState.team2.isReady)
            {
                Program.GameState.team1.currentPlayers = JsonSerializer.Deserialize<List<GameObject>>(JsonSerializer.Serialize(Program.GameState.team1.initialPalyers));
                Program.GameState.team2.currentPlayers = JsonSerializer.Deserialize<List<GameObject>>(JsonSerializer.Serialize(Program.GameState.team2.initialPalyers));
                Program.GameState.currentBall = JsonSerializer.Deserialize<GameObject>(JsonSerializer.Serialize(Program.GameState.initialBall)); ;

                Program.GameState.phase = Phase.PLAY;
                /*
                 * Program.GameState.CurrentBall = new GameObject
                {
                    Position = Program.GameState.InitialBall.Position,
                    Speed = Program.GameState.InitialBall.Speed,
                    Direction = Program.GameState.InitialBall.Direction
                    
                };
                */
                //.
            }
        }

        private static void UpdatePlayer(UpdatePlayers players)
        {
            Team team;

            if (Program.GameState.team1.id == players.Id)
            {
                team = Program.GameState.team1;
            }
            else if (Program.GameState.team2.id == players.Id)
            {
                team = Program.GameState.team2;
            }
            else
            {
                return;
            }
            foreach (var player in players.Players)
            {
                var cp = team.currentPlayers.Find(cp => cp.id == player.id);
                if (cp != null)
                {
                    player.direction.Normalise();
                    cp.direction = player.direction;
                    // Max speed check
                    if (player.speed > Program.GameState.PlayerMaxSpeed)
                    {
                        cp.speed = Program.GameState.PlayerMaxSpeed;
                    }
                    else
                    {
                        cp.speed = player.speed;
                    }
                }
            }

            //Player hit the ball if close enough
            if (players.ball != null)
            {
                var actorPlayer = team.currentPlayers.FirstOrDefault(p => p.id == players.ball.actorPlayerId);
                if (actorPlayer is not null && Point2d.Subtract(actorPlayer.position, Program.GameState.currentBall.position).Distance() <= GlobalConfiguration.ballDistance)
                {

                    Program.GameState.currentBall.direction = players.ball.direction;
                    Program.GameState.currentBall.direction.Normalise();
                    if (players.ball.speed > Program.GameState.BallMaxSpeed)
                    {
                        Program.GameState.currentBall.speed = Program.GameState.BallMaxSpeed;
                    }
                    else
                    {
                        Program.GameState.currentBall.speed = players.ball.speed;
                    }
                    
                }
            }


        }

        public async Task SendState()
        {
            Program.GameState.version = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();

            //Küldi a statet
            await SendMessageToAllAsync(JsonSerializer.Serialize(Program.GameState, options));
        }
    }

}
