﻿namespace WebFootball.Models
{
    public class GameObject
    {
        public int id { get; set; }
        public Point2d position { get; set; }
        public Point2d direction { get; set; }
        public float speed { get; set; }
    }
}
