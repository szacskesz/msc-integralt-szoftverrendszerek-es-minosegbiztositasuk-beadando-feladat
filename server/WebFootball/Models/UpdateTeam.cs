﻿using System.Collections.Generic;

namespace WebFootball.Models
{
    public class UpdateTeam : Message
    {
        public string Id { get; set; }
        public bool IsReady { get; set; }
        public List<GameObject> InitialPlayers { get; set; }
    }
}
