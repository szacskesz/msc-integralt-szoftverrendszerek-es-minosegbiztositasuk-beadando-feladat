﻿namespace WebFootball.Models
{
    public class Connect : Message
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
