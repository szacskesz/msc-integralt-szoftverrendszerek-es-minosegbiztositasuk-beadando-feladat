﻿using System.Collections.Generic;

namespace WebFootball.Models
{
    public class Team
    {
        public string id { get; set; }
        public string name { get; set; }
        public Side side { get; set; }
        public bool isReady { get; set; }
        public int score { get; set; }
        public List<GameObject> initialPalyers { get; set; }
        public List<GameObject> currentPlayers { get; set; }
    }
}
