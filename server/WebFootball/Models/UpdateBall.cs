﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebFootball.Models
{
    public class UpdateBall
    {
        public Point2d direction { get; set; }
        public float speed { get; set; }
        public int actorPlayerId { get; set; }

    }
}
