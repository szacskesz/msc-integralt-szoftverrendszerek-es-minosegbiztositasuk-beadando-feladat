﻿using WebFootball.Models;

namespace WebFootball
{
    public enum Phase
    {
        WAIT_FOR_CONNECTION,
        WAIT_FOR_READY,
        PLAY,
        WAIT_FOR_DISCONNECT
    }

    public class State
    {
        public long version { get; set; }
        public Phase phase { get; set; }
        public long remainingTime { get; set; }
        public Team team1 { get; set; }
        public Team team2 { get; set; }
        public GameObject initialBall { get; set; }
        public GameObject currentBall { get; set; }

        public float PlayerMaxSpeed { get; set; }

        public float BallMaxSpeed { get; set; }

        public string type { get; } = "STATE";
    }
}
