﻿using System;

namespace WebFootball.Models
{
    public class Point2d
    {
        public Point2d(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public Point2d() {}

        public double x { get; set; }
        public double y { get; set; }

        public void Normalise()
        {
            double max = Math.Max(x, y);
            double min = Math.Min(x, y);
            double distance = Distance();
            x /= distance;
            y /= distance;
        }

        public double Distance()
        {
            return Math.Sqrt((x * x) + (y * y));
        }

        public static Point2d Subtract(Point2d p1, Point2d p2)
        {
            return new Point2d(p1.x - p2.x, p1.y - p2.y);
        }
    }
}
