﻿using System.Collections.Generic;

namespace WebFootball.Models
{
    public class UpdatePlayers: Message
    {
        public string Id { get; set; }
        public List<GameObject> Players { get; set; }

        public UpdateBall ball { get; set; }
    }
}
