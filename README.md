# Int. szoft. és min. beadandó feladat.

Integrált szoftverrendszerek és minőségbiztosításuk (MSc) c. tárgyhoz elkészítendő beadandó feladat repository-ja.

# Résztvevők
| Név                        | Neptunkód |
|----------------------------|-----------|
| Szacsuri Norbert           | G0XXTC    |
| Szilvási Attila            | PIPLA5    |
| Szöllősi Dániel            | IU4MA4    |
| Mádi Viktor                | IR8C7K    |

# Feladat leírása
Egy robot foci alkalmazás elkészítése szerver-kliens architektúrával.
A program 2D-s felülnézetes foci játék, minimális funkciókkal.

# Elvárások
- Kliens-szerver architektúra
- Socket/websocket használata
- 1 szerver <-> 2 kliens kommunikáció kialakítása
- Mindent szerver oldalon kezelni
    - Gól
    - Passzolás
    - Mozgás

# Amik nem szükségesek
- Szabálytalanságok
- 3D-ből fakadó dolgok (pl.: ívelés, beadás)
- Kapufa

# Szerver
- Realtime feldolgozás és szimuláció
- Fogadja a kliensek csatlakozását valamilyen socket-en keresztül,
- Pontosan 2 kliensnek kell csatlakoznia
- Kezeli (elvégzi) a mozgásokat
    - Játékos,
    - Labda
- Klienstől kapott utasítások alapján megváltoztatja az adott játékosok:
    - Mozgási irányat
    - Sebességét, 
    - Labda irányát (csak ha interakció történik vele),
    - Labda sebességét (csak ha interakció történik vele).
- Folyamatosan küldi a klienseknek a játékosok és a labda állapotát
    - 120 tick/sec-enként
- A labda pozíciója és érkezési iránya alapján megállapítja ha gól született
- Idő kezelése
- Labda a játékos kezelése
- Labdaszerzés játékostól kezelése
- Fixálni a pálya méretét
- Deszikronizáció kezelése (kieső packet-ek)


# Kliens
- Saját játékosainak az állapotát változtatja (mozgás irány sebesség, labda interakció) a szerverre küldött utasítások által
- Szervertől kapott adatok alaján frissíti a játék állípotát
- Kezelnie kell formációkat, legalább kezdésnél,
    - Játékos szerepeket (kapus nem megy előre támadni, stb)
- Térfél alapján eldönteni melyik kapura kell lőni
- GUI

# GUI
- Minimalisztikus
- 2D
- Felülnézetes
- Játékoson körök
- Labda is kör (kisebb)
- Különböző színek (csapatok, labda, kapus)
- Javascriptben
- Csatlakozó képernyő (nevet meg kell adni csatlakozáshoz)
- Lobby (várakozás a másik félre, "ready" állapot kezelése)
- Meccs szimuláció képernyő

# Üzenet formátum
- JSON
    - ClientId (generált kliens azonosító)
    - Timestamp (időbélyeg) ()
    - Utasítások (tömb: 'playerModel'-ből ami módosítható és az ID)
