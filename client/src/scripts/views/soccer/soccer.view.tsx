import { createFpsMeter, updateFpsMeter } from '../components/pixi/fps-meter.component.pixi';
import { createGameObjects, updateGameObjects } from '../components/pixi/game-objects.component.pixi';
import { createSoccerField, updateSoccerField } from '../components/pixi/soccer-field.component.pixi';
import { View } from '../view.abstract';
import { Store } from '../../service/store.service';
import { updateScoreBoardJSX, ScoreBoardJSX } from '../components/jsx/scoreboard.component.jsx';
import { renderer } from '../../utils/jsx-renderer.utils';
import { ClientService } from '../../service/client.service';

class SoccerView extends View {

    protected doDraw = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement) => {
        createSoccerField(app);
        createFpsMeter(app);
        createGameObjects(app);

        renderer.render(
            <ScoreBoardJSX
                opacity={0.4}
                onmouseenter={(e: any) => {e.target.style.opacity="1.0"}}
                onmouseleave={(e: any) => {e.target.style.opacity="0.4"}}
            />
        ).on(htmlContainer);
            
        const gState = Store.getGameState();
        updateScoreBoardJSX(htmlContainer, {
            timeLeft: gState?.remainingTime,
            team1Name: gState?.team1?.name,
            team2Name: gState?.team2?.name,
            team1Score: gState?.team1?.score,
            team2Score: gState?.team2?.score,
        });
    }

    protected doUpdate = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement, delta: number) => {
        updateSoccerField(app);
        updateFpsMeter(app);
        updateGameObjects(app);

        const gState = Store.getGameState();
        updateScoreBoardJSX(htmlContainer, {
            timeLeft: gState?.remainingTime,
            team1Name: gState?.team1?.name,
            team2Name: gState?.team2?.name,
            team1Score: gState?.team1?.score,
            team2Score: gState?.team2?.score,
        })

        ClientService.decideMoves(delta);
    }
}

const soccerView = new SoccerView();
export {soccerView as SoccerView};
