import { ClientService } from '../../service/client.service';
import { convertFormDataToObject } from '../../utils/form-utils';
import { renderer } from '../../utils/jsx-renderer.utils';
import { DashboardJSX, updateDashboardJSX } from '../components/jsx/dashboard.component.jsx';
import { createSoccerField, updateSoccerField } from '../components/pixi/soccer-field.component.pixi';
import { View } from '../view.abstract';
import { v4 as uuid } from 'uuid';

class DashboardView extends View {

    protected doDraw = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement) => {
        createSoccerField(app);
        
        renderer.render(
            <DashboardJSX onsubmit={(event: any) => {
                const data = convertFormDataToObject(new FormData(event.target));
                if(data.teamName == null || data.teamName == "") return;
                if(data.url == null || data.url == "") return;
                
                ClientService.connect(data.url, uuid(), data.teamName)
                    .catch((e: any) => {
                        console.error(e);
                        alert("Could not connect to: " + data.url);
                    });
            }}/>
        ).on(htmlContainer);
    }
    
    protected doUpdate = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement, delta: number) => {
        updateSoccerField(app);

        updateDashboardJSX(htmlContainer, {});
    }
}

const dashboardView = new DashboardView();
export {dashboardView as DashboardView};
