import { renderer } from '../../utils/jsx-renderer.utils';
import { View } from '../view.abstract';
import { createSoccerField, updateSoccerField } from '../components/pixi/soccer-field.component.pixi';
import { ScoreBoardJSX, updateScoreBoardJSX } from '../components/jsx/scoreboard.component.jsx';
import { FullTimeTextJSX, updateFullTimeTextJSX } from '../components/jsx/full-time-text.component.jsx';
import { Store } from '../../service/store.service';

class FullTimeView extends View {

    protected doDraw = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement) => {
        createSoccerField(app);

        renderer.render(
            <div id="full-time">
                <ScoreBoardJSX />
                <FullTimeTextJSX />
            </div>
        ).on(htmlContainer);
            
        const gState = Store.getGameState();
        updateScoreBoardJSX(htmlContainer, {
            timeLeft: gState?.remainingTime,
            team1Name: gState?.team1?.name,
            team2Name: gState?.team2?.name,
            team1Score: gState?.team1?.score,
            team2Score: gState?.team2?.score,
        });
        updateFullTimeTextJSX(htmlContainer, {});
    }
    
    protected doUpdate = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement, delta: number) => {
        updateSoccerField(app);

        const gState = Store.getGameState();
        updateScoreBoardJSX(htmlContainer, {
            timeLeft: gState?.remainingTime,
            team1Name: gState?.team1?.name,
            team2Name: gState?.team2?.name,
            team1Score: gState?.team1?.score,
            team2Score: gState?.team2?.score,
        })
        updateFullTimeTextJSX(htmlContainer, {});
    }
}

const fullTimeView = new FullTimeView();
export {fullTimeView as FullTimeView};