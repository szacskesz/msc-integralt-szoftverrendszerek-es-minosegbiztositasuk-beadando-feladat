import * as PIXI from 'pixi.js'
import { fieldHeight, fieldWidth, grassLineCount } from '../../../utils/constants.utils';
import { getContainers } from '../../../utils/container.utils';
import { drawRect, drawLine, drawCircle } from '../../../utils/draw.utils';

export const grassLineRatio = 1/grassLineCount;
export const lineWidthRatio = 1/(20*grassLineCount);
export const grassWidth = fieldWidth * grassLineRatio;
export const lineWidth = fieldWidth * lineWidthRatio;

let grassLines: PIXI.Graphics[] = [];
let line_left: PIXI.Graphics;
let line_right: PIXI.Graphics;
let line_top: PIXI.Graphics;
let line_bottom: PIXI.Graphics;
let corner_top_left : PIXI.Graphics;
let corner_top_right : PIXI.Graphics;
let corner_bottom_left : PIXI.Graphics;
let corner_bottom_right : PIXI.Graphics;
let line_middle: PIXI.Graphics;
let center_spot: PIXI.Graphics;
let center_circle: PIXI.Graphics;
let left_penalty_spot: PIXI.Graphics;
let left_penalty_circle: PIXI.Graphics;
let left_penalty_box_bottom: PIXI.Graphics;
let left_penalty_box_side: PIXI.Graphics;
let left_penalty_box_top: PIXI.Graphics;
let left_keeper_box_bottom: PIXI.Graphics;
let left_keeper_box_side: PIXI.Graphics;
let left_keeper_box_top: PIXI.Graphics;
let left_goal: PIXI.Graphics;
let right_penalty_spot: PIXI.Graphics;
let right_penalty_circle: PIXI.Graphics;
let right_penalty_box_bottom: PIXI.Graphics;
let right_penalty_box_side: PIXI.Graphics;
let right_penalty_box_top: PIXI.Graphics;
let right_keeper_box_bottom: PIXI.Graphics;
let right_keeper_box_side: PIXI.Graphics;
let right_keeper_box_top: PIXI.Graphics;
let right_goal: PIXI.Graphics;

export function createSoccerField(app: PIXI.Application): void {
    const { dynamic: dynamicContainer } = getContainers(app);

    for (let i = 0; i < grassLineCount; i++) {
        let rect = drawRect(
            {x: i*grassWidth, y: 0},
            {x: (i+1)*grassWidth, y: fieldHeight},
            0,
            0xFFFFFF,
            1,
            true,
            (i % 2 == 0 ? 0x459500 : 0x5DAA00),
            1
        );
        grassLines.push(rect);
        dynamicContainer.addChild(rect);
    }

    line_left = drawLine(
        {x: grassWidth, y: grassWidth},
        {x: grassWidth, y: fieldHeight - grassWidth},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(line_left);

    line_middle = drawLine(
        {x: fieldWidth/2, y: grassWidth},
        {x: fieldWidth/2, y: fieldHeight - grassWidth},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(line_middle);

    line_right = drawLine(
        {x: fieldWidth - grassWidth, y: grassWidth},
        {x: fieldWidth - grassWidth, y: fieldHeight - grassWidth},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(line_right);

    line_top = drawLine(
        {x: grassWidth, y: grassWidth},
        {x: fieldWidth - grassWidth, y: grassWidth},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(line_top);

    line_bottom = drawLine(
        {x: grassWidth, y: fieldHeight - grassWidth},
        {x: fieldWidth - grassWidth, y: fieldHeight - grassWidth},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(line_bottom);

    corner_top_left = new PIXI.Graphics();
    corner_top_left.lineStyle(lineWidth, 0xFFFFFF);
    corner_top_left.arc(grassWidth, grassWidth, 2, 0, Math.PI / 2)
    dynamicContainer.addChild(corner_top_left)

    corner_top_right = new PIXI.Graphics();
    corner_top_right.lineStyle(lineWidth, 0xFFFFFF);
    corner_top_right.arc(fieldWidth - grassWidth, grassWidth, 2, Math.PI / 2, Math.PI)
    dynamicContainer.addChild(corner_top_right)

    corner_bottom_left = new PIXI.Graphics();
    corner_bottom_left.lineStyle(lineWidth, 0xFFFFFF);
    corner_bottom_left.arc(grassWidth, fieldHeight - grassWidth, 2, 3 * Math.PI / 2, 2 * Math.PI)
    dynamicContainer.addChild(corner_bottom_left)

    corner_bottom_right = new PIXI.Graphics();
    corner_bottom_right.lineStyle(lineWidth, 0xFFFFFF);
    corner_bottom_right.arc(fieldWidth - grassWidth, fieldHeight - grassWidth, 2, Math.PI, 3 * Math.PI / 2)
    dynamicContainer.addChild(corner_bottom_right)

    center_spot = drawCircle(
        {x: fieldWidth / 2, y: fieldHeight / 2},
        0.25,
        lineWidth,
        0xFFFFFF,
        1,
        true,
        0xFFFFFF,
        1
    );
    dynamicContainer.addChild(center_spot);

    center_circle = drawCircle(
        {x: fieldWidth / 2, y: fieldHeight / 2},
        9.15,
        lineWidth,
        0xFFFFFF,
        1,
        false,
        0xFFFFFF,
        1
    );
    dynamicContainer.addChild(center_circle);

    left_penalty_spot = drawCircle(
        {x: grassWidth + 11, y: fieldHeight / 2},
        0.25,
        lineWidth,
        0xFFFFFF,
        1,
        true,
        0xFFFFFF,
        1
    );
    dynamicContainer.addChild(left_penalty_spot);

    left_penalty_circle = new PIXI.Graphics();
    left_penalty_circle.lineStyle(lineWidth, 0xFFFFFF);
    left_penalty_circle.arc((grassWidth + 11), (fieldHeight / 2), 9.15, - 0.926, 0.926)
    dynamicContainer.addChild(left_penalty_circle)

    left_penalty_box_bottom = drawLine(
        {x: grassWidth, y: fieldHeight/2 + 40.3/2},
        {x: grassWidth + 16.5, y: fieldHeight/2 + 40.3/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(left_penalty_box_bottom);

    left_penalty_box_top = drawLine(
        {x: grassWidth, y: fieldHeight/2 - 40.3/2},
        {x: grassWidth + 16.5, y: fieldHeight/2 - 40.3/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(left_penalty_box_top);

    left_penalty_box_side = drawLine(
        {x: grassWidth + 16.5, y: fieldHeight/2 - 40.3/2},
        {x: grassWidth + 16.5, y: fieldHeight/2 + 40.3/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(left_penalty_box_side);

    left_keeper_box_bottom = drawLine(
        {x: grassWidth, y: fieldHeight/2 + (7.32 + 2 * 5.5)/2},
        {x: grassWidth + 5.5, y: fieldHeight/2 + (7.32 + 2 * 5.5)/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(left_keeper_box_bottom);

    left_keeper_box_top = drawLine(
        {x: grassWidth, y: fieldHeight/2 - (7.32 + 2 * 5.5)/2},
        {x: grassWidth + 5.5, y: fieldHeight/2 - (7.32 + 2 * 5.5)/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(left_keeper_box_top);

    left_keeper_box_side = drawLine(
        {x: grassWidth + 5.5, y: fieldHeight/2 - (7.32 + 2 * 5.5)/2},
        {x: grassWidth + 5.5, y: fieldHeight/2 + (7.32 + 2 * 5.5)/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(left_keeper_box_side);

    left_goal = drawRect(
        {x: grassWidth, y: fieldHeight/2 - (7.32)/2},
        {x: grassWidth/2, y: fieldHeight/2 + (7.32)/2},
        lineWidth,
        0xFFFFFF,
        1,
        true,
        0xFFFFFF,
        0.5
    );
    dynamicContainer.addChild(left_goal);

    right_penalty_spot = drawCircle(
        {x: fieldWidth - grassWidth - 11, y: fieldHeight / 2},
        0.25,
        lineWidth,
        0xFFFFFF,
        1,
        true,
        0xFFFFFF,
        1
    );
    dynamicContainer.addChild(right_penalty_spot);

    right_penalty_circle = new PIXI.Graphics();
    right_penalty_circle.lineStyle(lineWidth, 0xFFFFFF);
    right_penalty_circle.arc((fieldWidth - grassWidth - 11), (fieldHeight / 2), 9.15, Math.PI - 0.926, Math.PI + 0.926)
    dynamicContainer.addChild(right_penalty_circle)

    right_penalty_box_bottom = drawLine(
        {x: fieldWidth - grassWidth, y: fieldHeight/2 + 40.3/2},
        {x: fieldWidth - grassWidth - 16.5, y: fieldHeight/2 + 40.3/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(right_penalty_box_bottom);

    right_penalty_box_top = drawLine(
        {x: fieldWidth - grassWidth, y: fieldHeight/2 - 40.3/2},
        {x: fieldWidth - grassWidth - 16.5, y: fieldHeight/2 - 40.3/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(right_penalty_box_top);

    right_penalty_box_side = drawLine(
        {x: fieldWidth - grassWidth - 16.5, y: fieldHeight/2 - 40.3/2},
        {x: fieldWidth - grassWidth - 16.5, y: fieldHeight/2 + 40.3/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(right_penalty_box_side);

    right_keeper_box_bottom = drawLine(
        {x: fieldWidth - grassWidth, y: fieldHeight/2 + (7.32 + 2 * 5.5)/2},
        {x: fieldWidth - grassWidth - 5.5, y: fieldHeight/2 + (7.32 + 2 * 5.5)/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(right_keeper_box_bottom);

    right_keeper_box_top = drawLine(
        {x: fieldWidth - grassWidth, y: fieldHeight/2 - (7.32 + 2 * 5.5)/2},
        {x: fieldWidth - grassWidth - 5.5, y: fieldHeight/2 - (7.32 + 2 * 5.5)/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(right_keeper_box_top);

    right_keeper_box_side = drawLine(
        {x: fieldWidth - grassWidth - 5.5, y: fieldHeight/2 - (7.32 + 2 * 5.5)/2},
        {x: fieldWidth - grassWidth - 5.5, y: fieldHeight/2 + (7.32 + 2 * 5.5)/2},
        lineWidth,
        0xFFFFFF,
        1,
        true
    );
    dynamicContainer.addChild(right_keeper_box_side);

    right_goal = drawRect(
        {x: fieldWidth - grassWidth, y: fieldHeight/2 - (7.32)/2},
        {x: fieldWidth - grassWidth/2, y: fieldHeight/2 + (7.32)/2},
        lineWidth,
        0xFFFFFF,
        1,
        true,
        0xFFFFFF,
        0.5
    );
    dynamicContainer.addChild(right_goal);

}

export function updateSoccerField(app: PIXI.Application): void {
}