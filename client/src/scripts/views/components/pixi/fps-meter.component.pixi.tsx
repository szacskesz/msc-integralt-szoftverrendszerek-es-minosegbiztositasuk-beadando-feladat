import * as PIXI from 'pixi.js'
import { RunInterval } from '../../../utils/run-interval.utils';
import { getContainers } from '../../../utils/container.utils';

let runInterval: RunInterval;
let fpsText: PIXI.Text;

export function createFpsMeter(app: PIXI.Application): void {
    runInterval =  new RunInterval(1000);

    fpsText = new PIXI.Text(`FPS: ${Math.round(app.ticker.FPS * 100) / 100}` , {
        fill: "white",
        stroke: "black",
        fontSize: "15px",
        strokeThickness: 3,
    });
    fpsText.zIndex = 9999;
    fpsText.x = 0;
    fpsText.y = 0;

    const { static: staticContainer } = getContainers(app);
    staticContainer.addChild(fpsText);
}

export function updateFpsMeter(app: PIXI.Application): void {
    if(runInterval.checkAndUpdate()) {
        fpsText.text = `FPS: ${Math.round(app.ticker.FPS * 100) / 100}`;
    }
}