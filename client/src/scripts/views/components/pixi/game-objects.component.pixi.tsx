import * as PIXI from 'pixi.js'
import { Store } from '../../../service/store.service';
import { Object } from '../../../types/types';
import { getContainers } from '../../../utils/container.utils';
import { drawCircle } from '../../../utils/draw.utils';

const graphics: PIXI.Graphics[] = [];

export function createGameObjects(app: PIXI.Application): void {
    const { dynamic: dynamicContainer } = getContainers(app);
    const state = Store.getGameState();
   
    //draw team1 players
    for(let player of state.team1!.currentPlayers) {
        drawObject(app, dynamicContainer, player, 1.5, 0xFF2222);
    }

    //draw team2 players
    for(let player of state.team2!.currentPlayers) {
        drawObject(app, dynamicContainer, player, 1.5, 0x2222FF);
    }

    //draw ball
    drawObject(app, dynamicContainer, state.currentBall!, 0.75, 0xFF9000);
    
    Store.setGameState(state);
}

export function updateGameObjects(app: PIXI.Application): void {    
    const state = Store.getGameState();

    //update team1 players
    for(let player of state.team1!.currentPlayers) {
        updateObject(player);
    }

    //update team2 players
    for(let player of state.team1!.currentPlayers) {
        updateObject(player);
    }

    //update ball
    updateObject(state.currentBall!);

    Store.setGameState(state)
}

function drawObject(app: PIXI.Application, dynamicContainer: PIXI.Container, object: Object, radius: number, color: number) {
    object.graphicsIndex = graphics.length; 
    graphics[object.graphicsIndex] = drawCircle(
        { x: 0, y: 0 },
        radius,
        0.1,
        0x000000,
        1,
        true,
        color,
        1
    );
    graphics[object.graphicsIndex].x = object.position.x;
    graphics[object.graphicsIndex].y = object.position.y;
    
    dynamicContainer.addChild(graphics[object.graphicsIndex]);
}

function updateObject(object: Object) {
    if(object.graphicsIndex != null) {
        graphics[object.graphicsIndex].x = object.position.x;
        graphics[object.graphicsIndex].y = object.position.y;
    }
}