export type ScoreBoardJSXInitProps = {
    opacity?: number;
    onmouseenter?: ((e:any) => void);
    onmouseleave?: ((e:any) => void);
}

export type ScoreBoardJSXUpdateProps = {
    timeLeft?: number;
    team1Name?: string;
    team2Name?: string;
    team1Score?: number;
    team2Score?: number;
}

export const ScoreBoardJSX = (props: ScoreBoardJSXInitProps, renderer: any, children: any) => {
    const opacity = (props.opacity != null) ? props.opacity : 1.0;
    const onmouseenter = (props.onmouseenter != null) ? props.onmouseenter : (e: any) => {};
    const onmouseleave = (props.onmouseleave != null) ? props.onmouseleave : (e: any) => {};

    return (
        <div id="scoreboard">
            <div class="container" style={"opacity: " + opacity + ";"} onmouseenter={onmouseenter} onmouseleave={onmouseleave}>
                <div class="time" id="scoreboard-time" />
                <div class="part middle">
                    <div class="score-base score" id="scoreboard-score" />
                </div>
                <div class="part left">
                    <div class="team-name-base team-name left" id="scoreboard-team1name-1" />
                    <div class="hidden score-base" />
                    <div class="hidden team-name-base right" id="scoreboard-team1name-2" />
                </div>
                <div class="part right">
                    <div class="hidden team-name-base left" id="scoreboard-team2name-1" />
                    <div class="hidden score-base" />
                    <div class="team-name-base team-name right" id="scoreboard-team2name-2" />
                </div>
            </div>
        </div>
    );
}

export const updateScoreBoardJSX = (htmlContainer: HTMLElement, props: ScoreBoardJSXUpdateProps) => {
    props.timeLeft = props.timeLeft != null && props.timeLeft >= 0 ? props.timeLeft : 0;
    const timeLeft = props.timeLeft/1000;
    const minsLeft = Math.floor(timeLeft/60);
    const secsLeft = Math.floor(timeLeft % 60);
    const timeString = (minsLeft < 10 ? "0" : "") + minsLeft.toString() + ":" + (secsLeft < 10 ? "0" : "") + secsLeft.toString();
    
    props.team1Score = props.team1Score != null && props.team1Score >= 0 ? props.team1Score : 0;
    props.team2Score = props.team2Score != null && props.team2Score >= 0 ? props.team2Score : 0;
    const scoreString = (props.team1Score < 10 ? "" : "") + props.team1Score + " - " + (props.team2Score < 10 ? "" : "") + props.team2Score;

    props.team1Name = props.team1Name != null && props.team1Name.trim() != "" ? props.team1Name : "???";
    const team1NameString = props.team1Name;

    props.team2Name = props.team2Name != null && props.team2Name.trim() != "" ? props.team2Name : "???";
    const team2NameString = props.team2Name;

    document.getElementById("scoreboard-time")!.innerHTML = timeString;
    document.getElementById("scoreboard-score")!.innerHTML = scoreString;
    document.getElementById("scoreboard-team1name-1")!.innerHTML = team1NameString;
    document.getElementById("scoreboard-team1name-2")!.innerHTML = team1NameString;
    document.getElementById("scoreboard-team2name-1")!.innerHTML = team2NameString;
    document.getElementById("scoreboard-team2name-2")!.innerHTML = team2NameString;
}
