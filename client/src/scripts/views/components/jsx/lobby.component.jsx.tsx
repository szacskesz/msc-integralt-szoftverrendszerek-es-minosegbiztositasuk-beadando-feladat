import { State } from "../../../types/types";

export type LobbyJSXInitProps = {
    onReadyClick?: ((e:any) => void);
}

export type LobbyJSXUpdateProps = {
    teamId: string;
    state: State
}

export const LobbyJSX = (props: LobbyJSXInitProps, renderer: any, children: any) => {
    const onReadyClick = (props.onReadyClick != null) ? props.onReadyClick : (e: any) => {};

    return (
        <div id="lobby">
        <div class="container red">
            <div id="lobby-team1-name" class="name"></div>
            <div id="lobby-team1-status" class="status"></div>
            <div id="lobby-team1-button" class="button" style="visibility: hidden;">
                <button onclick={(e: any) => {e.preventDefault(); onReadyClick(e);}}>Ready</button>
            </div>
        </div>
        <div class="container blue">
            <div id="lobby-team2-name" class="name"></div>
            <div id="lobby-team2-status" class="status"></div>
            <div id="lobby-team2-button" class="button" style="visibility: hidden;">
                <button onclick={(e: any) => {e.preventDefault(); onReadyClick(e);}}>Ready</button>
            </div>
        </div>
    </div>
    );
}

export const updateLobbyJSX = (htmlContainer: HTMLElement, props: LobbyJSXUpdateProps) => {
    const {teamId, state} = props

    const team1NameString = state.team1 != null ? state.team1.name : "";
    const team2NameString = state.team2 != null ? state.team2.name : "";
    const team1StatusString = state.team1 != null ? state.team1.isReady ? "Ready!" : "Waiting for ready..." : "Waiting for connection...";
    const team2StatusString = state.team2 != null ? state.team2.isReady ? "Ready!" : "Waiting for ready..." : "Waiting for connection...";
    const team1ButtonVisible = state.team1 != null && state.team1.id === teamId && !state.team1.isReady;
    const team2ButtonVisible = state.team2 != null && state.team2.id === teamId && !state.team2.isReady;

    document.getElementById("lobby-team1-name")!.innerHTML = team1NameString;
    document.getElementById("lobby-team2-name")!.innerHTML = team2NameString;
    document.getElementById("lobby-team1-status")!.innerHTML = team1StatusString;
    document.getElementById("lobby-team2-status")!.innerHTML = team2StatusString;
    document.getElementById("lobby-team1-button")!.style.visibility = team1ButtonVisible ? "visible" : "hidden";
    document.getElementById("lobby-team2-button")!.style.visibility = team2ButtonVisible ? "visible" : "hidden";
}
