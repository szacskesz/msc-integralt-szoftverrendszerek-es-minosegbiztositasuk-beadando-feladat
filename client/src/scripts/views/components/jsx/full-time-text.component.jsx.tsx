export type FullTimeTextJSXInitProps = {
}

export type FullTimeTextJSXUpdateProps = {
}

export const FullTimeTextJSX = (props: FullTimeTextJSXInitProps, renderer: any, children: any) => {
    return (
        <div class="full-time-text-container">
            <div class="full-time-text">
                FULL TIME
            </div>
        </div>
    );
}

export const updateFullTimeTextJSX = (htmlContainer: HTMLElement, props: FullTimeTextJSXUpdateProps) => {
}
