export type DashboardJSXInitProps = {
    onsubmit?: ((e: any) => void)
}

export type DashboardJSXUpdateProps = {
}

export const DashboardJSX = (props: DashboardJSXInitProps, renderer: any, children: any) => {
    const onsubmit = (props.onsubmit != null) ? props.onsubmit : (e: any) => {};

    return (
        <div id="dashboard">
            <div class="container">
                <form onsubmit={(event: any) => {event.preventDefault(); onsubmit(event);}}>
                    <div class="header">
                        <img src="images/favicon.ico" />
                        <h2>Connect</h2>
                    </div>
                    <div class="fields">
                        <input type="text" name="url" placeholder="Server url" value={"ws://localhost:5000/ws"} />
                        <input type="text" name="teamName" placeholder="Team name" />
                    </div>
                    <div class="checkbox">
                        <input type="checkbox" name="checkbox" /><span>I agree to sell my soul.</span>
                    </div>
                    <button class="submit" type="submit">Submit</button>
                </form>
            </div>
        </div>
    );
}

export const updateDashboardJSX = (htmlContainer: HTMLElement, props: DashboardJSXUpdateProps) => {
}
