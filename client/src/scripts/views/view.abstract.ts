import * as PIXI from 'pixi.js'
import { Store } from '../service/store.service';
import { getContainers } from '../utils/container.utils';

export abstract class View {

    constructor() {}

    public draw = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement) => {
        const { dynamic: dynamicContainer, static: staticContainer } = getContainers(app);
        dynamicContainer.removeChildren();
        staticContainer.removeChildren();
        htmlContainer.innerHTML = "";
        htmlContainer.style.visibility = "false";

        this.doDraw(app, canvasContainer, htmlContainer);

        Store.setViewFunction(this.update);
    }

    protected abstract doDraw: (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement) => void;

    public update = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement, delta: number) => {
        this.doUpdate(app, canvasContainer, htmlContainer, delta);
    };

    protected abstract doUpdate: (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement, delta: number) => void;
}