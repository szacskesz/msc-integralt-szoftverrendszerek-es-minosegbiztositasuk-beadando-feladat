import { Standard_4_4_2_formation } from '../../formations/standard_4_4_2.formation';
import { ClientService } from '../../service/client.service';
import { Store } from '../../service/store.service';
import { renderer } from '../../utils/jsx-renderer.utils';
import { LobbyJSX, updateLobbyJSX } from '../components/jsx/lobby.component.jsx';
import { createSoccerField, updateSoccerField } from '../components/pixi/soccer-field.component.pixi';
import { View } from '../view.abstract';

class LobbyView extends View {

    protected doDraw = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement) => {
        createSoccerField(app);
        renderer.render(
            <LobbyJSX onReadyClick={(e: any) => { ClientService.updateTeam(true, Standard_4_4_2_formation); }} />
        ).on(htmlContainer);
    }
    
    protected doUpdate = (app: PIXI.Application, canvasContainer: HTMLElement, htmlContainer: HTMLElement, delta: number) => {
        updateSoccerField(app);
        updateLobbyJSX(htmlContainer, {teamId: Store.getId(), state: Store.getGameState()})
    }
}

const lobbyView = new LobbyView();
export {lobbyView as LobbyView};