import * as PIXI from 'pixi.js'
import { fieldHeight, fieldWidth } from './utils/constants.utils';
import { Store } from './service/store.service';
import { getContainers, initContainers } from './utils/container.utils';

let app: PIXI.Application;
let canvasContainer: HTMLElement;
let htmlContainer: HTMLElement;
const aspectRatio = fieldWidth/fieldHeight;

function main() {
    setCallbacks();
}

function setCallbacks() {
    window.onload = onLoad;
    window.onresize = onResize;
}

function onLoad() {
    app = new PIXI.Application({
        width: fieldWidth,
        height: fieldHeight,
        antialias: true,
        transparent: false,
    });
    app.renderer.plugins.interaction.moveWhenInside = true;

    canvasContainer = document.getElementById("canvas-container")!;
    htmlContainer = document.getElementById("html-container")!;

    canvasContainer.appendChild(app.view);
    initContainers(app);
    onResize();

    PIXI.Loader.shared.load(setup);
}

function onResize() {
    const {innerWidth: width, innerHeight: height} = window;

    const widthFromHeight = height * aspectRatio;
    const heightFromWidth = width / aspectRatio;

    const { dynamic: dynamicContainer } = getContainers(app);
    if(width > widthFromHeight) {
        app.renderer.resize(widthFromHeight, height);
        dynamicContainer.scale = new PIXI.Point(app.renderer.width/fieldWidth, app.renderer.height/fieldHeight);
    } else {
        app.renderer.resize(width, heightFromWidth);
        dynamicContainer.scale = new PIXI.Point(app.renderer.width/fieldWidth, app.renderer.height/fieldHeight);
    }

    htmlContainer.style.width = app.renderer.width + "px";
    htmlContainer.style.height = app.renderer.height + "px";
}

function setup() {
    app.ticker.maxFPS = 30;
    app.ticker.add((delta) => gameLoop(delta));

    // This should stay, initializes internal state
    Store.setGameState({
        phase: "WAIT_FOR_CONNECTION",
        version: -1
    });

    const teamId = "asd";
    Store.setId(teamId); 
}

function gameLoop(delta: number) {
    const ViewFunction = Store.getViewFunction();
    ViewFunction(app, canvasContainer, htmlContainer, delta);
}

main();
