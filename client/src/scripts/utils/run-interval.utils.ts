export class RunInterval {
    private _previousTime: number = 0;
    private _interval: number = 0;

    public constructor(interval: number = 1000) {
        this._previousTime = performance.now();
        this._interval = interval;
    }

    public get interval() {
        return this._interval;
    }

    public set interval(interval: number) {
        this._interval = interval;
    } 

    public checkAndUpdate() {
        const time = performance.now();
        const elapsedTime = time - this._previousTime;

        if(elapsedTime > this._interval) {
            this._previousTime = time;

            return true;
        } else {
            return false;
        }
    }
}
