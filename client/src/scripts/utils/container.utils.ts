import * as PIXI from 'pixi.js'

export const STATIC_CONTAINER_INDEX = 1;
export const DYNAMIC_CONTAINER_INDEX = 0;

export function initContainers(app: PIXI.Application): void {
    app.stage.addChildAt(new PIXI.Container(), DYNAMIC_CONTAINER_INDEX);
    app.stage.addChildAt(new PIXI.Container(), STATIC_CONTAINER_INDEX);
}

export function getContainers(app: PIXI.Application): {static: PIXI.Container, dynamic: PIXI.Container} {
    return {
        static: getStaticContainer(app),
        dynamic: getDynamicContainer(app),
    };
}

function getStaticContainer(app: PIXI.Application): PIXI.Container {
    return app.stage.getChildAt(STATIC_CONTAINER_INDEX) as PIXI.Container;
}

function getDynamicContainer(app: PIXI.Application): PIXI.Container {
    return app.stage.getChildAt(DYNAMIC_CONTAINER_INDEX) as PIXI.Container;
}