import * as PIXI from 'pixi.js'
import { normalise, subtract } from './vector.utils';
import { Point } from '../types/types';

export function drawRect(from: Point, to: Point, width: number = 1, color: number = 0xFFFFFF, colorAlpha: number = 1, fill: boolean = false, fillColor: number = 0xFFFFFF, fillColorAlpha: number = 1): PIXI.Graphics {
    let rect = new PIXI.Graphics();
    rect.lineStyle(width, color, colorAlpha);
    if(fill) rect.beginFill(fillColor, fillColorAlpha);
    rect.drawPolygon([
        from.x, from.y,
        from.x, to.y,
        to.x, to.y,
        to.x, from.y
    ]);
    if(fill) rect.endFill();

    return rect;
} 

export function drawLine(from: Point, to: Point, width: number = 1, color: number = 0xFFFFFF, colorAlpha: number = 1, addWidthToEndings: boolean = true): PIXI.Graphics {
    let start = from;
    let end = to;

    if(addWidthToEndings) {
        let diff = normalise(subtract(end, start));
        start.x = start.x - diff.x * width/2;
        start.y = start.y - diff.y * width/2;
        end.x = end.x + diff.x * width/2;
        end.y = end.y + diff.y * width/2;
    }
    
    let line = new PIXI.Graphics();
    line.lineStyle(width, color, colorAlpha);
    line.moveTo(start.x, start .y);
    line.lineTo(end.x, end.y);

    return line;
}

export function drawCircle(center: Point, radius: number, width: number = 1, color: number = 0xFFFFFF, colorAlpha: number = 1, fill: boolean = false, fillColor: number = 0xFFFFFF, fillColorAlpha: number = 1) : PIXI.Graphics {
    let circle = new PIXI.Graphics();
    circle.lineStyle(width, color, colorAlpha)
    if(fill) circle.beginFill(fillColor, fillColorAlpha);
    circle.drawCircle(center.x, center.y, radius);
    if(fill) circle.endFill();

    return circle;
}
