
export function objectWithoutProperties<T>(obj: T, propsToExclude: (keyof T)[]) {
    let objClone = {...obj};
    
    for(let prop in objClone) {
        if(propsToExclude.includes(prop)) {
            delete objClone[prop];
        }
    }

    return objClone;
} 

export function prettyPrintObject(obj: any, indent = 2, currentIndentation = 0) {
    let string = "";

    if(obj == null) return "null";

    for(let prop in obj) {
        if(obj[prop] == null) {
            string += "\n"
            for(let i = 0; i < (indent + currentIndentation); i++) {
                string += " ";
            }
            string += prop + ": null";
        }

        switch(typeof obj[prop]) {
            case "string":
            case "number":
            case "bigint":
            case "symbol":
            case "boolean": {
                string += "\n"
                for(let i = 0; i < (indent + currentIndentation); i++) {
                    string += " ";
                }
                string += prop + ": " + obj[prop];
                break;
            }
            case "object": {
                string += "\n"
                for(let i = 0; i < (indent + currentIndentation); i++) {
                    string += " ";
                }
                string += prop + ": ";
                let strO = prettyPrintObject(obj[prop], indent, currentIndentation + indent);
                if(strO.length > 0) string += "\n" + strO;

                break;
            }
        }
    }

    return string.substring(1);
}