import { Point } from '../types/types';

/**
 * v1-v2
 * @param v1 
 * @param v1 
 */
export function subtract(v1: Point, v2: Point): Point {
    return {
        x: v1.x - v2.x,
        y: v1.y - v2.y
    };
}

export function length2(v: Point): number {
    return ((v.x*v.x) + (v.y*v.y));
}

export function length(v: Point): number {
    return Math.sqrt(length2(v));
}

export function normalise(v: Point): Point {
    let l = length(v);
    if(l == 0) throw new Error("Zero lenth vector cannot be normalised!");

    return {
        x: v.x / l,
        y: v.y / l
    }
}

export function dotProduct(v1: Point, v2: Point): number {
    return v1.x * v2.x + v1.y * v2.y;
}