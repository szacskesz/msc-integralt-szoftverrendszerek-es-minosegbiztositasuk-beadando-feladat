
export function fromJsonOrNull(json: string) {
    let obj: any;
    try{
        obj = JSON.parse(json);
    } catch(e) {
        obj = null;
    }

    return obj;
}