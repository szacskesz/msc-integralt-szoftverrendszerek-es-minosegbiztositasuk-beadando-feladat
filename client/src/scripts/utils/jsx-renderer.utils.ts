import { CommonDOMRenderer } from 'render-jsx/dom';

export const renderer = new CommonDOMRenderer(); // note: do not rename renderer, because the jsx compiler looks for this name!