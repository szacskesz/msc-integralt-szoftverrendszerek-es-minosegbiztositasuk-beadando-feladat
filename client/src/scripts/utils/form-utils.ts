export function convertFormDataToObject(formData: FormData) {
    let object: {[key: string]: string} = {};
    
    if(formData != null) {
        formData.forEach(function(value, key){
            object[key] = value as string;
        });
    }

    return object;
}