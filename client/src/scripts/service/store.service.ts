import { State } from "../types/types";
import { DashboardView } from "../views/dashboard/dashboard.view";
import { FullTimeView } from "../views/full-time/full-time.view";
import { LobbyView } from "../views/lobby/lobby.view";
import { SoccerView } from "../views/soccer/soccer.view";
import { View } from "../views/view.abstract";

class Store {
    private viewFunction: View["update"] | null = null;
    private id: string | null = null;
    private state: State | null = null;

    setViewFunction(fn: View["update"]) {
        this.viewFunction = fn;
    }

    getViewFunction(): View["update"] {
        if(this.viewFunction == null) throw new Error("View Function is null!");
    
        return this.viewFunction;
    }

    /**
     * Sets the saved team id.
     * @param newId the new team id.
     */
    setId(newId: string) {
        this.id = newId;
    }

    /**
     * Gets the team id.
     */
    getId(): string {
        if(this.id == null) throw new Error("Id is null in store!");

        return this.id;
    }

    setGameState(newState: State) {
        //copy graphicsIndex
        if(this.state != null && newState != null) {
            this.copyPropertyIfPossible(this.state.currentBall, newState.currentBall, "graphicsIndex", false);
            
            if(this.state.team1 != null && this.state.team1.currentPlayers != null && newState.team1 != null && newState.team1.currentPlayers != null) {
                for(let player of this.state.team1.currentPlayers) {
                    let newPlayer = newState.team1.currentPlayers.find(p => p.id === player.id);
                    this.copyPropertyIfPossible(player, newPlayer, "graphicsIndex", false);
                }
            }
    
            if(this.state.team2 != null && this.state.team2.currentPlayers != null && newState.team2 != null && newState.team2.currentPlayers != null) {
                for(let player of this.state.team2.currentPlayers) {
                    let newPlayer = newState.team2.currentPlayers.find(p => p.id === player.id);
                    this.copyPropertyIfPossible(player, newPlayer, "graphicsIndex", false);
                }
            }
        }

        this.state = newState;

        switch(newState.phase) {
            case "WAIT_FOR_CONNECTION": {
                const id = this.id

                if(id == null) {
                    if(this.viewFunction != DashboardView.update && this.viewFunction != DashboardView.draw) {
                        this.viewFunction = DashboardView.draw;
                    }
                } else {
                    if(this.viewFunction != LobbyView.update && this.viewFunction != LobbyView.draw) {
                        this.viewFunction = LobbyView.draw;
                    }
                }

                break;
            }
            case "WAIT_FOR_READY": {
                if(this.viewFunction != LobbyView.update && this.viewFunction != LobbyView.draw) {
                    this.viewFunction = LobbyView.draw;
                }
                break;
            }
            case "PLAY": {
                if(this.viewFunction != SoccerView.update && this.viewFunction != SoccerView.draw) {
                    this.viewFunction = SoccerView.draw;
                }
                break;
            }
            case "WAIT_FOR_DISCONNECT": {
                if(this.viewFunction != FullTimeView.update && this.viewFunction != FullTimeView.draw) {
                    this.viewFunction = FullTimeView.draw;
                }
                break;
            }
        }
    }
    
    /**
     * For websocket usage, to compare newState and currentState versions.
     * Should be faster than getting the whole state.
     */
    getGameStateVersion(): number {
        if(this.state == null) throw new Error("State is null in store!");

        return this.state.version;
    }
    
    getGameState(): State {
        if(this.state == null) throw new Error("State is null in store!");

        return JSON.parse(JSON.stringify(this.state)); // disallow modifying internal state directly
    }
    
    private copyPropertyIfPossible<T>(from: T|undefined|null, to: T|undefined|null, prop: keyof T, overwrite: boolean) {
        if(from != null && to != null) {
            if(!overwrite && to[prop] != null) {
                return;
            }
    
            to[prop] = from[prop]
        }
    }
}

const store = new Store();
export {store as Store};
