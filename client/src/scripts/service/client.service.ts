import { objectWithoutProperties } from "../utils/object.utils";
import { Store } from "./store.service";
import { ConnectMsg, StateMsg, Team, UpdatePlayersMsg, UpdateTeamMsg } from "../types/types";
import { WebsocketService } from "./websocket.service";
import { fromJsonOrNull } from "../utils/serialize.utils";
import { Formation } from "../formations/formation.interface";


export const TIMEOUT_MS = 10*1000;
class ClientService {

    async connect(url: string, teamId: string, teamName: string) {
        return new Promise<void>((resolve, reject) => {
            const timeout = setTimeout(() => {reject()}, TIMEOUT_MS);

            WebsocketService.connect(
                url,
                (error) => {
                    console.error(error);
                },
                (message) => {
                    let stateObj: {type: string} = fromJsonOrNull(message.data);

                    if(stateObj != null && stateObj.type != null) {
                        switch(stateObj.type) {
                            case "STATE": {
                                const stateMsg = stateObj as StateMsg;
    
                                let currentVersion = Store.getGameStateVersion();
                                if(currentVersion == null || currentVersion < stateMsg.version) {
                                    Store.setGameState(objectWithoutProperties(stateMsg, ["type"]))
                                }
    
                                clearTimeout(timeout);
                                resolve();
                                break;
                            }
                        }
                    }
                }
            ).then(() => {
                const connectMsg: ConnectMsg = {
                    type : "CONNECT",
                    id: teamId,
                    name: teamName,
                }
        
                WebsocketService.sendMessage(connectMsg);
                Store.setId(teamId);
            });
        })
    }

    async updateTeam(isReady: boolean, formation: Formation) {
        const tId = Store.getId();
        const gState = Store.getGameState();

        let team: Team;
        if(gState.team1 != null && gState.team1.id === tId) {
            team = gState.team1
        } else if(gState.team2 != null && gState.team2.id === tId) {
            team = gState.team2;
        } else {
            throw new Error("Team not found!");
        }

        const updateTeamMsg: UpdateTeamMsg = {
            type: "UPDATE_TEAM",
            id: tId,
            initialPlayers: formation.getFormation(team.side),
            isReady: isReady,
        }
        await WebsocketService.sendMessage(updateTeamMsg);
    }

    async decideMoves(delta: number) {
        const teamId = Store.getId();
        const currentState = Store.getGameState();

        let team: Team;
        if(currentState.team1 != null && currentState.team1.id === teamId) {
            team = currentState.team1
        } else if(currentState.team2 != null && currentState.team2.id === teamId) {
            team = currentState.team2;
        } else {
            throw new Error("Team not found!");
        }
        
        // TODO this is only an example
        // update players direction & speed then send it to server
        // you can safely modify team because getGameState returns a copy of the current state.
        let updateTeamMsg: UpdatePlayersMsg = {
            type: "UPDATE_PLAYERS",
            id: team.id,
            players: [team.currentPlayers[0]].map(cp => {
                return {
                    id: cp.id,
                    direction: {x: 1, y: 0},
                    speed: 0.03,
                };
            }),
            // players: [],
            ball: {
                actorPlayerId: team.currentPlayers[0].id,
                direction: {x: 1, y: 0},
                speed: 0.1,
            }
        }

        console.warn({msg: "UDPATE", delta, updateTeamMsg})
        await WebsocketService.sendMessage(updateTeamMsg);
    }

    async disconnect() {
        await WebsocketService.disconnect();
    }
}

const clientService = new ClientService();
export {clientService as ClientService};
