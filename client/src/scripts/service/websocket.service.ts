
class WebsocketService {
    private ws: WebSocket | null = null;

    connect(url: string, errorHandler: ((error: any) => void), messageHandler: ((message: any) => void)): Promise<void> {
        return new Promise((resolve, reject) => {
            try{
                if(this.ws != null) reject(new Error("Websocket already connected!"));

                this.ws = new WebSocket(url);
                this.ws.onopen = () => {
                    this.ws!.onopen = null;
                    this.ws!.onerror = errorHandler;
                    this.ws!.onmessage = messageHandler;
                    this.ws!.onclose = null;
        
                    resolve();
                };
                this.ws.onerror = (e) => {
                    this.ws!.onopen = null;
                    this.ws!.onerror = null;
                    this.ws!.onmessage = null;
                    this.ws!.onclose = null;
                    this.ws = null;
        
                    reject(e);
                };
            } catch(e) {
                reject(e);
            }
        });
    }
    
    sendMessage(msg: any): Promise<void> {
        return new Promise((resolve, reject) => {
            if(this.ws == null) reject(new Error("Websocket not yet connected!"));
            if(this.ws!.readyState != WebSocket.OPEN) reject(new Error("Websocket connection error"));
    
            this.ws!.send(JSON.stringify(msg));
            resolve();
        });
    }
    
    setErrorHandler(errorHandler: ((error: any) => void)): Promise<void> {
        return new Promise((resolve, reject) => {
            if(this.ws == null) reject(new Error("Websocket not yet connected!"));
    
            this.ws!.onerror = errorHandler;
            resolve();
        });
    }
    
    setMessageHandler(messageHandler: ((message: any) => void)): Promise<void> {
        return new Promise((resolve, reject) => {
            if(this.ws == null) reject(new Error("Websocket not yet connected!"));
    
            this.ws!.onmessage = messageHandler;
            resolve();
        });
    }
    
    disconnect(): Promise<void> {
        return new Promise((resolve, reject) => {
            if(this.ws == null) reject(new Error("Websocket not yet connected!"));
            if(this.ws!.readyState != WebSocket.OPEN) reject(new Error("Websocket connection error"));
            
            const method = () => {
                this.ws!.onopen = null;
                this.ws!.onerror = null;
                this.ws!.onmessage = null;
                this.ws!.onclose = null;
                this.ws = null;
    
                resolve();
            };
    
            this.ws!.onclose = method;
            this.ws!.onerror = method;
    
            this.ws!.close();
        });
    
    }
}

const websocketService = new WebsocketService();
export {websocketService as WebsocketService};