import { Object } from "../types/types";
import { Formation } from "./formation.interface";

class Standard_4_4_2_formation implements Formation {
    getFormation(side: "LEFT" | "RIGHT"): Object[] {
        switch(side) {
            case "LEFT": return this.getLeftFormation();
            case "RIGHT": return this.getRightFormation();
        }

    }

    private getLeftFormation(): Object[] {
        return[
            {id: 1, position: {x: 6, y: 45}, direction: {x: 0, y: 0}, speed: 0}, // keeper
            {id: 2, position: {x: 30, y: 15}, direction: {x: 0, y: 0}, speed: 0}, // defender
            {id: 3, position: {x: 25, y: 35}, direction: {x: 0, y: 0}, speed: 0}, // defender
            {id: 4, position: {x: 25, y: 55}, direction: {x: 0, y: 0}, speed: 0}, // defender
            {id: 5, position: {x: 30, y: 75}, direction: {x: 0, y: 0}, speed: 0}, // defender
            {id: 6, position: {x: 55, y: 15}, direction: {x: 0, y: 0}, speed: 0}, // middfielder
            {id: 7, position: {x: 45, y: 35}, direction: {x: 0, y: 0}, speed: 0}, // middfielder
            {id: 8, position: {x: 45, y: 55}, direction: {x: 0, y: 0}, speed: 0}, // middfielder
            {id: 9, position: {x: 55, y: 75}, direction: {x: 0, y: 0}, speed: 0}, // middfielder
            {id: 10, position: {x: 62, y: 39}, direction: {x: 0, y: 0}, speed: 0}, // striker
            {id: 11, position: {x: 62, y: 51}, direction: {x: 0, y: 0}, speed: 0}, // striker
        ];
    }

    private getRightFormation(): Object[] {
        return[
            {id: 1, position: {x: 134, y: 45}, direction: {x: 0, y: 0}, speed: 0}, // keeper
            {id: 2, position: {x: 110, y: 75}, direction: {x: 0, y: 0}, speed: 0}, // defender
            {id: 3, position: {x: 115, y: 55}, direction: {x: 0, y: 0}, speed: 0}, // defender
            {id: 4, position: {x: 115, y: 35}, direction: {x: 0, y: 0}, speed: 0}, // defender
            {id: 5, position: {x: 110, y: 15}, direction: {x: 0, y: 0}, speed: 0}, // defender
            {id: 6, position: {x: 85, y: 75}, direction: {x: 0, y: 0}, speed: 0}, // middfielder
            {id: 7, position: {x: 95, y: 55}, direction: {x: 0, y: 0}, speed: 0}, // middfielder
            {id: 8, position: {x: 95, y: 35}, direction: {x: 0, y: 0}, speed: 0}, // middfielder
            {id: 9, position: {x: 85, y: 15}, direction: {x: 0, y: 0}, speed: 0}, // middfielder
            {id: 10, position: {x: 78, y: 51}, direction: {x: 0, y: 0}, speed: 0}, // striker
            {id: 11, position: {x: 78, y: 39}, direction: {x: 0, y: 0}, speed: 0}, // striker
        ];
    }
}

const standard_4_4_2_formation = new Standard_4_4_2_formation();
export {standard_4_4_2_formation as Standard_4_4_2_formation};
