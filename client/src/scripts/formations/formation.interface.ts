import { Object } from "../types/types";

export interface Formation {
    getFormation(side: "LEFT" | "RIGHT"): Object[];
}